#Configure OpenSSL
NDK_ROOT="/home/yaaminu/Android/Ndk"
SSLPATH="/home/yaaminu/Downloads/openssl-1.1.0c"
SCRIPTPATH=$PWD
NDK_PROJECT_PATH=$SCRIPTPATH
cd $SSLPATH
./Configure android no-asm no-shared no-cast no-idea no-camellia no-whirpool
EXITCODE=$?
if [ $EXITCODE -ne 0 ]; then
    echo "Error running the ssl configure program"
    cd $PWD
    exit $EXITCODE
fi

$NDK_ROOT/ndk-build -j$JOBS -C $SCRIPTPATH crypto
EXITCODE=$?
if [ $EXITCODE -ne 0 ]; then
    echo "Error building libcrypto"
    cd $PWD
    exit $EXITCODE
fi
