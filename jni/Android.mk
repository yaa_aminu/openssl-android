LOCAL_PATH := $(call my-dir)

#Crypto
include $(LOCAL_PATH)/crypto.mk

#Static libcrypto
include $(CLEAR_VARS)
LOCAL_MODULE := crypto
LOCAL_SRC_FILES := $(CRYPTO_LOCAL_SRC_FILES)
LOCAL_C_INCLUDES := $(CRYPTO_LOCAL_C_INCLUDES)
LOCAL_CPPFLAGS += -std=c++11
include $(LOCAL_PATH)/optimizations.mk
LOCAL_CFLAGS += $(CRYPTO_COMMON_CFLAGS)
include $(BUILD_STATIC_LIBRARY)
